##############################
ECG Feature Extraction Toolbox
##############################

.. image:: https://mybinder.org/badge_logo.svg
 :target: https://mybinder.org/v2/gl/pwamej%2Fecg-features-extraction/master?urlpath=lab/tree/notebook.ipynb
.. image:: https://img.shields.io/badge/code%20style-black-000000.svg
    :target: https://github.com/python/black

Toolbox for extracting features from ECG heart electrical activity data.

***********
Development
***********

Prerequisites
=============

- Pipenv_
- Python 3.7 or pyenv_ installed and configured

Setting up
==========
::

  pipenv install --dev
  pipenv shell
  python -m ipykernel install --user --name=ecg-feature-extraction
  pre-commit install

Launching notebook server
=========================
::

  jupyter lab

.. _Pipenv: https://docs.pipenv.org/en/latest/
.. _pyenv: https://github.com/pyenv/pyenv
