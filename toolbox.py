#!/usr/bin/env python3

import numpy as np
import pandas as pd
import pyarrow as pa
import pyarrow.parquet as pq
import pendulum
import statistics
import json
import click

from biosppy.signals import ecg

SAMPLING_RATE_HZ = 1000
REACTION_TIME_MS = 250


@click.command()
@click.argument("data_path", type=click.Path(exists=True))
@click.argument("metadata_path", type=click.Path(exists=True))
@click.option("--timezone", "metadata_timezone", default="Europe/Warsaw", help="Timezone of metadata timestamps.")
def main(data_path, metadata_path, metadata_timezone):
    """Segment and extract features from pair of ECG time series and events metadata.

    Returns result as Parquet file to standard output."""
    data = pd.read_csv(data_path, sep=", ", engine="python")
    metadata = pd.read_csv(metadata_path, header=None, names=["event", "timestamp"])
    metadata.timestamp = metadata.timestamp.apply(to_timestamp(metadata_timezone))
    last_timestamp = data.timestamp.values[-1]
    if metadata.timestamp.values[-1] < last_timestamp:
        end_event = {"event": "end", "timestamp": last_timestamp}
        metadata = metadata.append(end_event, ignore_index=True)
    windows = pd.DataFrame(
        {
            "event": metadata.event.values[:-1],
            "windowStart": metadata.timestamp.values[:-1] + REACTION_TIME_MS,
            "windowEnd": metadata.timestamp.values[1:] + REACTION_TIME_MS,
        }
    )
    segmented_data = windows.apply(lambda x: data[data.timestamp.between(x.windowStart, x.windowEnd)], axis="columns")
    features = pd.DataFrame([extract_features(segment) for segment in segmented_data])
    result = pd.concat([windows, features], axis="columns")
    table = pa.Table.from_pandas(result)
    buf = pa.BufferOutputStream()
    pq.write_table(table, buf)
    click.echo(buf.getvalue().to_pybytes(), nl=False)


def extract_features(signal):
    r_positions = ecg.engzee_segmenter(signal.value.values, SAMPLING_RATE_HZ)[0]
    rr_intervals = r_positions[1:] - r_positions[:-1]
    rr_mean = np.mean(rr_intervals)
    diffs = rr_intervals[1:] - rr_intervals[:-1]
    return {
        "mad": np.median(np.abs(rr_intervals - rr_mean)),
        "sdnn": np.std(rr_intervals),
        "rmssd": np.sqrt(np.mean(diffs ** 2)),
        "sdsd": np.std(diffs),
        "pnn20": len(diffs[abs(diffs) > 20]) / len(diffs),
        "pnn50": len(diffs[abs(diffs) > 50]) / len(diffs),
    }


def to_timestamp(timezone):
    return lambda x: int(pendulum.parse(x, tz=timezone).timestamp() * 1000)


if __name__ == "__main__":
    main()
